**About me**

## Hi, I'm Wanderson Maracaipe  
- 💻 I am FullStack Developer
- 💼 Front End Web Developer at [Maxdata](https://maxdatasistemas.com.br/)
- ❤️ I love PHP/Laravel and TypeScript
- 📚 Studying and improving in Angular, .NET Core, PHP, Laravel/Phalcon and Delphi
- 📫 Contact: wandersonmaracaipe@hotmail.com

**Languages and Tools:**

<code><img height="20" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/dotnetcore/dotnetcore-original.svg"></code>
<code><img height="20" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg"></code>
<code><img height="20" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/typescript/typescript-original.svg"></code>
<code><img height="20" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/angularjs/angularjs-original.svg"></code>
<code><img height="20" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/php/php-original.svg"></code>
<code><img height="20" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/laravel/laravel-plain.svg"></code>
<code><img height="20" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/phalcon/phalcon-original.svg"></code>
<code><img height="20" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg"></code>
<code><img height="20" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg"></code>
<code><img height="20" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-plain.svg"></code> 


<br />

  <a href="https://instagram.com/wmaracaipe" target="_blank"><img src="https://img.shields.io/twitter/url?label=Instagram&logo=instagram&style=social&url=https%3A%2F%2Finstagram.com%2Fwmaracaipe" target="_blank"></a> <a href="https://www.linkedin.com/in/wandersonmaracaipe/" target="_blank"><img src="https://img.shields.io/twitter/url?label=Linkedin&logo=Linkedin&style=social&url=https%3A%2F%2Fwww.linkedin.com%2Fin%2Fwandersonmaracaipe%2F" target="_blank"></a>  

